package com.aequilibrium.transformers.service;

import com.aequilibrium.transformers.dao.Transformer;
import com.aequilibrium.transformers.dao.TransformerRepository;
import com.aequilibrium.transformers.dto.BattleDTO;
import com.aequilibrium.transformers.dto.TransformerDTO;
import com.aequilibrium.transformers.utilities.ApiUtilities;
import com.aequilibrium.transformers.utilities.TransformerType;
import org.apache.commons.collections4.IterableUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.api.mockito.PowerMockito;
import org.mockito.Matchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(PowerMockRunner.class)
public class TransformersServiceTest {

    @InjectMocks
    private TransformerService transformersService;
    @Mock
    ApiUtilities apiUtilities;
    @Mock
    TransformerRepository transformerRepository;

    @Test
   public void testAdd_success() throws Exception {
         TransformerDTO transformerDTO=setupTransformerDTO("Optimus Prime");
         Transformer transformer=setupTransformer("Optimus");
        PowerMockito.when(apiUtilities.wrapDomainToDTO(Matchers.any(Transformer.class))).thenReturn(transformerDTO);
         PowerMockito.when(transformerRepository.save(Matchers.any(Transformer.class))).thenReturn(transformer);
         TransformerDTO transformerDTONew=transformersService.addTransformer(transformerDTO);
         Assert.assertEquals(transformerDTONew.getName(),transformerDTO.getName());
    }

    @Test
    public void testAdd_failure(){
        TransformerDTO transformerDTO=setupTransformerDTO("Optimus Prime");
        PowerMockito.when(apiUtilities.wrapDomainToDTO(Matchers.any(Transformer.class))).thenReturn(null);
        TransformerDTO transformerDTONew=transformersService.addTransformer(transformerDTO);
        Assert.assertNull(transformerDTONew);
    }

    @Test
    public void testRemove_success(){
       Assert.assertEquals(transformersService.removeTransformer(Matchers.anyLong()),true);
    }

    @Test
    public void testUpdate_success(){
        TransformerDTO transformerDTO=setupTransformerDTO("Optimus Prime");
        Transformer transformer=setupTransformer("Optimus");
        PowerMockito.when(transformerRepository.findById(Matchers.anyLong())).thenReturn((Optional.of(transformer)));
        PowerMockito.when(apiUtilities.wrapDomainToDTO(Matchers.any(Transformer.class))).thenReturn(null);
        PowerMockito.when(apiUtilities.wrapDomainToDTO(Matchers.any(Transformer.class))).thenReturn(transformerDTO);
        TransformerDTO transformerDTONew=transformersService.updateTransformer(transformerDTO);

        Assert.assertNotNull(transformerDTONew);
    }

    @Test
    public void testUpdate_failure(){
        TransformerDTO transformerDTO=setupTransformerDTO("Optimus Prime");
        Transformer transformer=setupTransformer("Optimus");
        PowerMockito.when(transformerRepository.findById(Matchers.anyLong())).thenReturn((Optional.of(transformer)));
        PowerMockito.when(apiUtilities.wrapDomainToDTO(Matchers.any(Transformer.class))).thenReturn(null);
        PowerMockito.when(apiUtilities.wrapDomainToDTO(Matchers.any(Transformer.class))).thenReturn(null);
        TransformerDTO transformerDTONew=transformersService.updateTransformer(transformerDTO);

        Assert.assertNull(transformerDTONew);
    }

    @Test
    public void testList(){
        Transformer transformer=setupTransformer("Optimus Prime");
        List<Transformer> transformerList=new ArrayList<>();
        transformerList.add(transformer);
        Iterable transformerDTOIterable=transformerList;
        PowerMockito.when(transformerRepository.findAll()).thenReturn(transformerDTOIterable);
        List<TransformerDTO> transformerDTONew=transformersService.listTransformers();
        Assert.assertEquals(transformerDTONew.size(),1);
    }


    @Test
    public void testList_Empty(){
        List<Transformer> transformerList=new ArrayList<>();
        Iterable transformerDTOIterable=transformerList;
        PowerMockito.when(transformerRepository.findAll()).thenReturn(transformerDTOIterable);
        List<TransformerDTO> transformerDTONew=transformersService.listTransformers();
        Assert.assertEquals(transformerDTONew.size(),0);
    }

    @Test
    public void testBattle_Optimus_Winner(){
        Transformer transformer=setupTransformer("Optimus Prime");
        Transformer transformer1=setupTransformerDecp("SoundWave");
        TransformerDTO transformerDTO=setupTransformerDTO("Optimus Prime");
        TransformerDTO transformerDTO1=setupTransformerDTO("SoundWave");

        List<Transformer> transformerList=new ArrayList<>();
        transformerList.add(transformer);
        transformerList.add(transformer1);
        List<Long> idList=new ArrayList<>();
        idList.add(1l );
        Iterable transformerDTOIterable=transformerList;
        PowerMockito.when(apiUtilities.wrapDomainToDTO(Matchers.any(Transformer.class))).thenReturn(transformerDTO, transformerDTO1);
        PowerMockito.when(transformerRepository.findAllById(Matchers.anyCollection())).thenReturn(transformerDTOIterable);
        BattleDTO battleDTO =transformersService.battle(idList);
        Assert.assertEquals(battleDTO.getNoOfBattles(),1);
        Assert.assertEquals(battleDTO.getWinningTeamMembers().size(),1);
        Assert.assertEquals(battleDTO.getWinningTeamMembers().get(0).getName(),"Optimus Prime");

    }

    @Test
    public void testBattle_Predaking_Winner(){
        Transformer transformer=setupTransformer("HubCup");
        Transformer transformer1=setupTransformerDecp("Predaking");
        TransformerDTO transformerDTO=setupTransformerDTO("HubCup");
        TransformerDTO transformerDTO1=setupTransformerDTO("Predaking");

        List<Transformer> transformerList=new ArrayList<>();
        transformerList.add(transformer);
        transformerList.add(transformer1);
        List<Long> idList=new ArrayList<>();
        idList.add(1l );
        Iterable transformerDTOIterable=transformerList;
        PowerMockito.when(apiUtilities.wrapDomainToDTO(Matchers.any(Transformer.class))).thenReturn(transformerDTO, transformerDTO1);
        PowerMockito.when(transformerRepository.findAllById(Matchers.anyCollection())).thenReturn(transformerDTOIterable);
        BattleDTO battleDTO =transformersService.battle(idList);
        Assert.assertEquals(battleDTO.getNoOfBattles(),1);
        Assert.assertEquals(battleDTO.getWinningTeamMembers().size(),1);
        Assert.assertEquals(battleDTO.getWinningTeamMembers().get(0).getName(),"Predaking");

    }
    private TransformerDTO setupTransformerDTO(String name){
        TransformerDTO transformerDTO=new TransformerDTO();
        transformerDTO.setHighestOverallRating(11l);
        transformerDTO.setName(name);
        transformerDTO.setTransformerType(TransformerType.AUTOBOT.getName());
        transformerDTO.setStrength(11l);
        transformerDTO.setCourage(11l);
        transformerDTO.setSpeed(11l);
        transformerDTO.setSkill(11l);
        transformerDTO.setRank(11l);
        transformerDTO.setIntelligence(11l);
        transformerDTO.setFirepower(11l);

        transformerDTO.setEndurance(11l);
       return transformerDTO;
    }

    private Transformer setupTransformer(String name){
        Transformer transformer=new Transformer();
        transformer.setName(name);
        transformer.setTransformerType(TransformerType.AUTOBOT.getName());
        transformer.setStrength(11l);
        transformer.setCourage(11l);
        transformer.setSpeed(11l);
        transformer.setSkill(11l);
        transformer.setRank(11l);
        transformer.setFirepower(11l);

        transformer.setIntelligence(11l);
        transformer.setEndurance(11l);
        return transformer;
    }

    private Transformer setupTransformerDecp(String name){
        Transformer transformer=new Transformer();
        transformer.setName(name);
        transformer.setTransformerType(TransformerType.DECPTICON.getName());
        transformer.setStrength(11l);
        transformer.setCourage(11l);
        transformer.setSpeed(11l);
        transformer.setSkill(11l);
        transformer.setRank(11l);
        transformer.setFirepower(11l);

        transformer.setIntelligence(11l);
        transformer.setEndurance(11l);
        return transformer;
    }
}
