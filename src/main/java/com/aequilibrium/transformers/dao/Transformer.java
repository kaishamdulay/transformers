package com.aequilibrium.transformers.dao;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "Transformers")
@Data
public class Transformer {
    @Id
    @GeneratedValue(
            generator = "Id"
    )
    @Column(
            name = "ID"
    )
    private Long id;

    @Column(
            name = "name"
    )
    private String name;

    @Column(
            name = "STRENGTH"
    )
    private Long strength;

    @Column(
            name = "INTELLIGENCE"
    )
    private Long intelligence;

    @Column(
            name = "SPEED"
    )
    private Long speed ;

    @Column(
            name = "ENDURANCE"
    )
    private Long endurance;

    @Column(
            name = "RANK"
    )
    private Long rank;

    @Column(
            name = "COURAGE"
    )
    private Long courage;

    @Column(
            name = "FIREPOWER"
    )
    private Long firepower;

    @Column(
            name = "SKILL"
    )
    private Long skill;

    @Column(
            name = "TRANSFORMER_TYPE"
    )
    private String transformerType;


}
