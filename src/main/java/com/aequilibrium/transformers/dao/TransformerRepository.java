package com.aequilibrium.transformers.dao;

import org.springframework.data.repository.CrudRepository;

import javax.annotation.Resource;

@Resource
public interface TransformerRepository extends CrudRepository<Transformer,Long> {

}
