package com.aequilibrium.transformers.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class ApiResponse<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private Boolean success = Boolean.TRUE;
    private int status = 200;
    private String message;
    private T body;
    private String errorCode;

    @Override
    public String toString() {
        return "ShariAPIResponse{" +
                "success=" + success +
                ", status=" + status +
                ", message='" + message + '\'' +
                ", body=" + body +
                ", errorCode='" + errorCode + '\'' +
                '}';
    }
}
