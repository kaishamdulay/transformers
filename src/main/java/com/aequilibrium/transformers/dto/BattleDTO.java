package com.aequilibrium.transformers.dto;

import lombok.Data;

import java.util.List;

@Data
public class BattleDTO {

    int noOfBattles;
    List<TransformerDTO> winningTeamMembers;
    List<TransformerDTO> loosingTeamRemainingMembers;
    List<Long> ids;
}
