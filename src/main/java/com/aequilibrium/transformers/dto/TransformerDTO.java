package com.aequilibrium.transformers.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@NoArgsConstructor
public class TransformerDTO {

    Long id;

    String name;

    private Long strength;


    private Long intelligence;


    private Long speed ;


    private Long endurance;


    private Long rank;


    private Long courage;


    private Long firepower;

    private Long skill;

    private String transformerType;

    private Long highestOverallRating;


}
