package com.aequilibrium.transformers.rest;

import com.aequilibrium.transformers.dao.Transformer;
import com.aequilibrium.transformers.dto.ApiResponse;
import com.aequilibrium.transformers.dto.BattleDTO;
import com.aequilibrium.transformers.dto.TransformerDTO;
import com.aequilibrium.transformers.service.TransformerService;
import com.aequilibrium.transformers.utilities.ApiUtilities;
import com.aequilibrium.transformers.utilities.ResponseEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TransformerRestService {

    @Autowired
    TransformerService transformerService;
    @Autowired
    ApiUtilities apiUtilities;

    @PostMapping("/add")
    public ApiResponse addTransformer(@RequestBody TransformerDTO transformerDTO){
        return  apiUtilities.getApiResponse(ResponseEnum.SUCCESS.getId(),Boolean.TRUE,"Added successfully",transformerService.addTransformer(transformerDTO));
    }


    @PostMapping("/remove")
    public ApiResponse removeTransformer(@RequestParam(value = "id") Long id){
        boolean deleteSuccess=transformerService.removeTransformer(id);
        if(deleteSuccess) {
            return apiUtilities.getApiResponse(ResponseEnum.SUCCESS.getId(), Boolean.TRUE, "Removed successfully transformer with ", id);
        }else{
            return apiUtilities.getApiResponse(ResponseEnum.NO_DATA_FOUND_ERROR.getId(), Boolean.FALSE, "Could not remove ", id);
        }

    }

    @GetMapping("/list")
    public ApiResponse listTransformers(){
        return  apiUtilities.getApiResponse(ResponseEnum.SUCCESS.getId(),Boolean.TRUE,"Added successfully",transformerService.listTransformers());
    }


    @PostMapping("/update")
    public ApiResponse updateTransformer(@RequestBody TransformerDTO transformerDTO){
        TransformerDTO transformerDTONew=transformerService.updateTransformer(transformerDTO);
        if(transformerDTONew!=null) {
            return apiUtilities.getApiResponse(ResponseEnum.SUCCESS.getId(), Boolean.TRUE, "Updated successfully", transformerDTO);
        }else{
            return apiUtilities.getApiResponse(ResponseEnum.NO_DATA_FOUND_ERROR.getId(), Boolean.FALSE, "Could not update ", transformerDTO.getId());
        }
    }


    @PostMapping("/battle")
    public ApiResponse updateTransformer(@RequestBody BattleDTO battleDTO){
        BattleDTO battleDTONew=transformerService.battle(battleDTO.getIds());
        return  apiUtilities.getApiResponse(ResponseEnum.SUCCESS.getId(),Boolean.TRUE,"Added successfully",battleDTONew);
    }
}
