package com.aequilibrium.transformers.service;

import com.aequilibrium.transformers.dao.Transformer;
import com.aequilibrium.transformers.dao.TransformerRepository;
import com.aequilibrium.transformers.dto.BattleDTO;
import com.aequilibrium.transformers.dto.TransformerDTO;
import com.aequilibrium.transformers.utilities.ApiUtilities;
import com.aequilibrium.transformers.utilities.TransformerType;
import lombok.extern.apachecommons.CommonsLog;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
@CommonsLog
public class TransformerService {

    @Autowired
    ApiUtilities apiUtilities;
    @Autowired
    TransformerRepository transformerRepository;

    private static final String OPTIMUS_PRIME = "Optimus Prime";
    private static final String PREDAKING = "Predaking";


    public TransformerDTO addTransformer(TransformerDTO transformerDTO) {
        Transformer transformer = apiUtilities.wrapDTOtoDomain(transformerDTO);
        try {
            transformer = transformerRepository.save(transformer);
        }catch (Exception e){
            log.error("Error adding data ",e);
        }
        return apiUtilities.wrapDomainToDTO(transformer);
    }


    public TransformerDTO updateTransformer(TransformerDTO transformerDTO) {
        Transformer transformer=null;
        try {
            transformer = transformerRepository.findById(transformerDTO.getId()).orElse(null);
        }catch(Exception e){
            log.error("Error updating data ",e);
        }
        apiUtilities.wrapDTOtoDomain(transformer, transformerDTO);
        if(transformer!=null) {
            transformerRepository.save(transformer);
        }
        return apiUtilities.wrapDomainToDTO(transformer);
    }

    public List<TransformerDTO> listTransformers() {
        Iterable<Transformer> transformerList = transformerRepository.findAll();
        List<TransformerDTO> transformerDTOList = new ArrayList<>();
        for (Transformer transformer : transformerList) {
            TransformerDTO transformerDTO = apiUtilities.wrapDomainToDTO(transformer);
            transformerDTOList.add(transformerDTO);
        }
        return transformerDTOList;
    }

    public boolean removeTransformer(Long id) {
        if(id!=null) {
            try {
                transformerRepository.deleteById(id);
            }catch (Exception e){
                return false;
            }
            return true;
        }else
        {
            return false;
        }
    }

    public BattleDTO battle(List<Long> ids) {

        Iterable<Long> idsItr = ids;
        BattleDTO battleDTO = new BattleDTO();
        Iterable<Transformer> transformerList = transformerRepository.findAllById(idsItr);
        PriorityQueue<TransformerDTO> autobotPriorityQueue = new PriorityQueue<>(Comparator.comparingLong(TransformerDTO::getRank).reversed());
        PriorityQueue<TransformerDTO> decepticonPriorityQueue = new PriorityQueue<>(Comparator.comparingLong(TransformerDTO::getRank).reversed());
        List<TransformerDTO> autobotList = new ArrayList<>();
        List<TransformerDTO> decepticonList = new ArrayList<>();


        if(!IterableUtils.isEmpty(transformerList)) {
            for (Transformer transformer : transformerList) {
                if (transformer.getTransformerType().equalsIgnoreCase(TransformerType.AUTOBOT.getName())) {
                    autobotPriorityQueue.add(calculateMaxRating(apiUtilities.wrapDomainToDTO(transformer)));
                } else {
                    decepticonPriorityQueue.add(calculateMaxRating(apiUtilities.wrapDomainToDTO(transformer)));
                }
            }
            int maxBattles=0;

            if(!CollectionUtils.isEmpty(decepticonPriorityQueue) && !CollectionUtils.isEmpty(autobotPriorityQueue)) {
                maxBattles = autobotPriorityQueue.size() > decepticonPriorityQueue.size() ? decepticonPriorityQueue.size() : autobotPriorityQueue.size();
                startBattle(maxBattles, autobotPriorityQueue, decepticonPriorityQueue, autobotList, decepticonList);

                if (autobotList.size() > decepticonList.size()) {
                    battleDTO.setWinningTeamMembers(autobotList);
                    battleDTO.setLoosingTeamRemainingMembers(decepticonList);
                    while (decepticonPriorityQueue.peek() != null) {
                        decepticonList.add(decepticonPriorityQueue.poll());
                    }
                } else if (decepticonList.size() > autobotList.size()) {
                    battleDTO.setWinningTeamMembers(decepticonList);
                    battleDTO.setLoosingTeamRemainingMembers(autobotList);
                    while (autobotPriorityQueue.peek() != null) {
                        autobotList.add(autobotPriorityQueue.poll());
                    }
                }
            }
            else if(!CollectionUtils.isEmpty(decepticonPriorityQueue) && CollectionUtils.isEmpty(autobotList)){
                while (decepticonPriorityQueue.peek() != null) {
                    decepticonList.add(decepticonPriorityQueue.poll());
                }
                battleDTO.setWinningTeamMembers(decepticonList);
            } else if(CollectionUtils.isEmpty(decepticonPriorityQueue) && !CollectionUtils.isEmpty(autobotList)){
                while (autobotPriorityQueue.peek() != null) {
                    autobotList.add(autobotPriorityQueue.poll());
                }
                battleDTO.setWinningTeamMembers(autobotList);
            }
            battleDTO.setNoOfBattles(maxBattles);
        }
        return battleDTO;
    }

    private TransformerDTO calculateMaxRating(TransformerDTO transformerDTO) {
        transformerDTO.setHighestOverallRating(transformerDTO.getStrength() + transformerDTO.getIntelligence()
                + transformerDTO.getEndurance() + transformerDTO.getFirepower());
        return transformerDTO;
    }

    private void startBattle(int maxBattles, PriorityQueue<TransformerDTO> autobotPriorityQueue, PriorityQueue<TransformerDTO> decepticonPriorityQueue, List<TransformerDTO> autobotList, List<TransformerDTO> decepticonList) {
        for (int index = 0; index < maxBattles; index++) {
            if (autobotPriorityQueue.peek().getName().equalsIgnoreCase(decepticonPriorityQueue.peek().getName())) {
                autobotList=new ArrayList<>();
                decepticonList=new ArrayList<>();
                break;
            } else if (autobotPriorityQueue.peek().getName().equalsIgnoreCase(OPTIMUS_PRIME)
                    && decepticonPriorityQueue.peek().getName().equalsIgnoreCase(PREDAKING)) {
                autobotList=new ArrayList<>();
                decepticonList=new ArrayList<>();
                break;
            } else if (decepticonPriorityQueue.peek().getName().equalsIgnoreCase(OPTIMUS_PRIME)
                    && autobotPriorityQueue.peek().getName().equalsIgnoreCase(PREDAKING)) {
                autobotList=new ArrayList<>();
                decepticonList=new ArrayList<>();
                break;
            } else if (autobotPriorityQueue.peek().getName().equalsIgnoreCase(OPTIMUS_PRIME)) {
                decepticonPriorityQueue.poll();
                autobotList.add(autobotPriorityQueue.poll());
            } else if (decepticonPriorityQueue.peek().getName().equalsIgnoreCase(OPTIMUS_PRIME)) {
                autobotPriorityQueue.poll();
                decepticonList.add(decepticonPriorityQueue.poll());
            } else if (autobotPriorityQueue.peek().getName().equalsIgnoreCase(PREDAKING)) {
                decepticonPriorityQueue.poll();
                autobotList.add(autobotPriorityQueue.poll());
            } else if (decepticonPriorityQueue.peek().getName().equalsIgnoreCase(PREDAKING)) {
                autobotPriorityQueue.poll();
                decepticonList.add(decepticonPriorityQueue.poll());
            } else if (autobotPriorityQueue.peek().getCourage() - decepticonPriorityQueue.peek().getCourage() >= 4
                    && autobotPriorityQueue.peek().getStrength() - decepticonPriorityQueue.peek().getStrength() >= 3) {
                decepticonPriorityQueue.poll();
                autobotList.add(autobotPriorityQueue.poll());
            } else if (decepticonPriorityQueue.peek().getCourage() - autobotPriorityQueue.peek().getCourage() >= 4
                    && decepticonPriorityQueue.peek().getStrength() - autobotPriorityQueue.peek().getStrength() >= 3) {
                autobotPriorityQueue.poll();
                decepticonList.add(decepticonPriorityQueue.poll());
            } else if (autobotPriorityQueue.peek().getSkill() - decepticonPriorityQueue.peek().getSkill() >= 3) {
                decepticonPriorityQueue.poll();
                autobotList.add(autobotPriorityQueue.poll());
            } else if (decepticonPriorityQueue.peek().getSkill() - autobotPriorityQueue.peek().getSkill() >= 3) {
                autobotPriorityQueue.poll();
                decepticonList.add(decepticonPriorityQueue.poll());
            } else if (autobotPriorityQueue.peek().getHighestOverallRating() > decepticonPriorityQueue.peek().getHighestOverallRating()) {
                decepticonPriorityQueue.poll();
                autobotList.add(autobotPriorityQueue.poll());
            } else if (decepticonPriorityQueue.peek().getHighestOverallRating() > autobotPriorityQueue.peek().getHighestOverallRating()) {
                autobotPriorityQueue.poll();
                decepticonList.add(decepticonPriorityQueue.poll());
            } else if (decepticonPriorityQueue.peek().getHighestOverallRating() == autobotPriorityQueue.peek().getHighestOverallRating()) {
                autobotPriorityQueue.poll();
                decepticonPriorityQueue.poll();
            }
        }


    }
}
