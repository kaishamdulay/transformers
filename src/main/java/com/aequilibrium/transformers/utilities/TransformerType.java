package com.aequilibrium.transformers.utilities;

public enum TransformerType {

    AUTOBOT(1, "A"),
    DECPTICON(2, "D");

    Integer id;
    String name;

    private TransformerType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "{\"id\":\"" + this.id + "\",\"name\":\"" + this.name + "\"}";
    }
}
