package com.aequilibrium.transformers.utilities;

import com.aequilibrium.transformers.dao.Transformer;
import com.aequilibrium.transformers.dto.ApiResponse;
import com.aequilibrium.transformers.dto.TransformerDTO;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Component;

@CommonsLog
@Component
public class ApiUtilities {

    public <T> ApiResponse<T> getApiResponse(int statusCode, Boolean success, String message, T responseObj) {
        log.debug("Start custom API response");
        ApiResponse<T> response = new ApiResponse<>();
        if (responseObj != null) {
            response.setBody(responseObj);
        }
        response.setSuccess(success);
        response.setStatus(statusCode);
        response.setMessage(message);

        log.debug("Finish custom API response");
        return response;
    }


    public Transformer wrapDTOtoDomain(TransformerDTO transformerDTO){
        Transformer transformer=new Transformer();
        transformer.setCourage(transformerDTO.getCourage());
        transformer.setEndurance(transformerDTO.getEndurance());
        transformer.setFirepower(transformerDTO.getFirepower());
        transformer.setIntelligence(transformerDTO.getIntelligence());
        transformer.setRank(transformerDTO.getRank());
        transformer.setSkill(transformerDTO.getSkill());
        transformer.setSpeed(transformerDTO.getSpeed());
        transformer.setStrength(transformerDTO.getStrength());
        transformer.setTransformerType(transformerDTO.getTransformerType());
        transformer.setName(transformerDTO.getName());
        return  transformer;
    }

    public void wrapDTOtoDomain(Transformer transformer,TransformerDTO transformerDTO){
        if(transformer!=null) {
            transformer.setCourage(transformerDTO.getCourage());
            transformer.setEndurance(transformerDTO.getEndurance());
            transformer.setFirepower(transformerDTO.getFirepower());
            transformer.setIntelligence(transformerDTO.getIntelligence());
            transformer.setRank(transformerDTO.getRank());
            transformer.setSkill(transformerDTO.getSkill());
            transformer.setSpeed(transformerDTO.getSpeed());
            transformer.setStrength(transformerDTO.getStrength());
            transformer.setTransformerType(transformerDTO.getTransformerType());
            transformer.setName(transformerDTO.getName());
        }
    }

    public TransformerDTO wrapDomainToDTO(Transformer transformer){
        if(transformer!=null) {
            TransformerDTO transformerDTO = new TransformerDTO();
            transformerDTO.setCourage(transformer.getCourage());
            transformerDTO.setEndurance(transformer.getEndurance());
            transformerDTO.setFirepower(transformer.getFirepower());
            transformerDTO.setIntelligence(transformer.getIntelligence());
            transformerDTO.setRank(transformer.getRank());
            transformerDTO.setSkill(transformer.getSkill());
            transformerDTO.setSpeed(transformer.getSpeed());
            transformerDTO.setStrength(transformer.getStrength());
            transformerDTO.setTransformerType(transformer.getTransformerType());
            transformerDTO.setName(transformer.getName());
            transformerDTO.setId(transformer.getId());
            return transformerDTO;
        }
        return null;
    }
}
